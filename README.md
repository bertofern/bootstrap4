### Curso 1 - Diseñando páginas web con Bootstrap 4 - 

## Instación y Uso

$ npm install

**Para generar una distribución en Gulp:**
**Se requiere tener instalado gulp-cli, sass y node-sass en modo global: $ npm i -g gulp-cli sass node-sass**

$ gulp build

**Para ver el proyecto y su distribución en Gulp:**

$ gulp

**Para ver unicamente el proyecto via lite-server:**

$ npm run dev

---

## About this proyect

*Prácticas del Módulo 1:*

1. Diseñar e implementar ese diseño de un sitio web responsive utilizando NPM,
2. Configurar tu repositorio utilizando Git y Bitbucket e
3. Implementar sistemas de grillas de Bootstrap.

*Puntos:*

1. El archivo .gitignore para evitar hacer seguimiento de los cambios en el directorio node_modules.
2. El script dev sobre el package.json que debe inicializar el server lite-server
3. Las referencias css y js a bootstrap, jquery y popper utilizando referencias a los archivos correspondientes de la carpeta node_modules sobre la página principal (index.html) , en el body, un elemento jumbotron con el título de su proyecto.
4. Un container con 3 párrafos utilizando los elementos p y un título con descripción a cada uno contando las principales características del producto.
5. Dentro del container y separado de los párrafos previos, una lista de productos que incluya un título, descripción y un botón, esta vez utilizando el sistema de grillas con filas y columnas. Y todo esto, ¿es responsive?
6. El sistema de grillas donde ubicó los productos transformado a flex
7. Una sección de footer en la página principal que incluye información del comercio, redes sociales y algún otro detalle referido al sitio.
8. En el caso de la dirección comercial utilizando el componente address
9. En el archivo css creado, utiliza estilos que sean replicables para todos los productos del listado principal, de forma tal de cambiar ese diseño en el css y cambien todos los productos?
10. Las clases de control de alineación para ubicar los elementos del footer a la izquierda, centro y derecha

---

*Prácticas del Módulo 2:*

1. Construir elementos de navegación en un sitio web.
2. Mostrar información utilizando tablas, tarjetas y media.
3. Construir formularios y botones para interacturar con los usuarios

*Puntos:*

1. La barra de navegación queda fija en la parte superior, es decir, que por más que te deslices hacia abajo, la barra siempre permanece visible al tope de la página.
2. La versión responsive de la barra que se ve bien en los diferentes dispositivos.
3. Los componentes breadcrumbs en las diferentes páginas, vinculando a las páginas correspondientes y ubicando al usuario en el mapa de sitio que corresponde.
4. Un formulario para dejar un comentario, entrar en contacto con el sitio o suscribirse a algún servicio o compra que se ofrezca en el sitio. Los labels agrupados con los controles correspondientes, y manteniendo la alineación y ubicación de los elementos de forma armónica. El formulario incluye campos de: email, de listado seleccionable, checkbox, texto y textarea.
5. La librería open-iconic y algunos iconos en la tabla que ilustran la información
6. El contenido de la tabla que puede verse con scroll sin deformar la página.
7. En la página principal, el listado de productos utiliza el componente card con los títulos, cuerpo y footer.
8. La disposición de los elementos (márgenes y padding, atributos de flex, etc.) es prolija y moderna.
9. Una imagen en cada una de las cards que representen el artículo que estás ofreciendo.
10. Una imagen en la página principal a modo de banner o encabezado que haga atractivo el sitio o muestre una promoción. Utiliza los componentes adecuados para este propósito.

---

*Prácticas del Módulo 3:*

1. Crear comportamiento dinámico utilizando Javascript
2. Crear elementos de navegación utilizando pestañas y menús desplegables
3. Crear alertas y vistas modales y un carousel.

*Puntos:*

1. Tabs o pills en alguna sección del sitio. Por ejemplo, tabla comparativa, opciones de pago, información para la compra, etc. y su versión responsive se ve correctamente.
2. Secciones que muestran información utilizando el componente collapse y multi collapse.
3. El componente accordion en alguna sección donde muestre un listado de opciones (p. ej. opciones de pago con tarjetas en la sección de precios).
4. Información de contexto via tooltips o popovers en los títulos de los productos mostrados en las cards.
5. Un componente modal sobre el final del body que invite al usuario a dejar sus datos o a registrarse.
6. Un formulario para que el usuario complete en el cuerpo del modal.
7. Un botón que abra el modal.
8. Un componente carousel en la parte superior de la página principal, que muestre algo merecedor de notoriedad.
9. Controles de desplazamiento sobre el carousel.
10. Un script que modifique la velocidad de desplazamiento a tu criterio.

---

*Prácticas del Módulo 4:*

1. Construir componentes de Bootstrap utilizando JQuery y Javascript
2. Crear CSS utilizando preprocesadores Sass y Less
3. Automatizar tareas con scripts de NPM
4. Preparar tu sitio para subirlo a un servidor

*Puntos:*

1. En la página donde está definido un modal, un script que registra por console las activaciones de todos los eventos del modal (cuando comienza a abrirse, cuando se terminó de abrir, cuando comienza a ocultarse y cuando se terminó de ocultar).
2. El botón que activa el modal se inhabilita y cambia de color sobre el evento de apertura del modal.
3. El botón que activa el modal se vuelve a activar y pasa al color original sobre el evento de cierre del modal
4. Un script npm en el archivo package.json con nombre “scss” que su objetivo sea revisar la carpeta donde se guardarán todos los archivos scss y compilarlos generando la versión css en la carpeta correspondiente. En el video tutorial lo hacemos así: “node-sass -o css/ css/”.
5. Una variable que represente el color amarillo, otra el rojo y otra el verde.
6. Un mixin en el archivo styles.scss que contenga varios atributos como ser: definiciones de márgenes, padding y colores.
7. El archivo styles.css en la carpeta que especificaste como output.
8. El proyecto preparado en la salida a producción con NPM Scripts, con Task Runners Grunt o con Task Runners Gulp, dentro de una carpeta identificada como tal (en los tutoriales se llamó “dist”).

