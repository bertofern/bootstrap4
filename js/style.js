$(document).ready(function () {
    if ($(window).width() > 991) {
        $('.navbar-light .d-menu').hover(function () {
            $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
        }, function () {
            $(this).find('.sm-menu').first().stop(true, true).delay(120).slideUp(100);
        });
    }
});

$("#collapse").click(function (e) {
    var el = document.getElementById("navbar4")
    if (el.classList.contains("show")) {
        el.classList.remove("show");
    } else {
        el.classList.add("show");
    }
});

$('[data-toggle="tooltip"]').tooltip()
$('[data-toggle="popover"]').popover()

$('.carousel').carousel({
    interval: 2000
})

$('#modal').on('show.bs.modal', function (e) {
    console.log('El modal se está mostrando')
    $('#modal-btn').removeClass('btn-dark')
    $('#modal-btn').addClass('btn-primary')
    $('#modal-btn').prop('disabled', true);
})
$('#modal').on('shown.bs.modal', function (e) {
    console.log('El modal se mostró')
})
$('#modal').on('hide.bs.modal', function (e) {
    console.log('El modal se está ocultando')
    $('#modal-btn').prop('disabled', false);
    $('#modal-btn').removeClass('btn-primary')
    $('#modal-btn').addClass('btn-dark')
})
$('#modal').on('hidden.bs.modal', function (e) {
    console.log('El modal se ocultó')
})

